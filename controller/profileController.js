const httpStatus = require('http-status')
const { Profile } = require('../models')

const imagekit = require('../lib/imageKit')
const catchAsync = require('../utils/catchAsync')
const ApiError = require('../utils/ApiError')

const updateProfile = catchAsync(async (req, res) => {
    const id = req.params.id

    const { address, city } = req.body
    const files = req.files

    const profile = await Profile.findByPk(id)
    // kondisi gagal update foto
    if (!profile) {
        throw new ApiError(httpStatus.NOT_FOUND, `profile with this id ${id} not found`)
    }

    // validasi utk format file image
    // const validFormat = file.mimetype == 'image/png' || file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/gif';
    // if (!validFormat) {
    //     throw new ApiError(httpStatus.BAD_REQUEST, 'Wrong Image Format')
    // }

    req.body.images = []
    await Promise.all(
        files.map(async (file) => {
            // untuk dapat extension file nya
            const split = file.originalname.split('.')
            const ext = split[split.length - 1]

            // upload file ke imagekit
            const img = await imagekit.upload({
                file: file.buffer, //required
                fileName: `IMG-${Date.now()}.${ext}`, //required
            })
            req.body.images.push(img.url)
        })
    )


    await Profile.update(
        {
            address,
            city,
            images: req.body.images
        },
        {
            where: {
                id
            }
        }
    )

    res.status(200).json({
        status: 'Success',
        message: 'successfully update profile'
    })
})

module.exports = {
    updateProfile
}


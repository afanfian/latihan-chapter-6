const router = require('express').Router()

// controller
const Profile = require('../controller/profileController')

// middleware
const Authentication = require('../middlewares/authenticate')
const Uploader = require('../middlewares/uploader')


router.put('/:id', Authentication, Uploader.array('images'), Profile.updateProfile)

module.exports = router